<?php
    $student = array("Ime: Karlo", "Prezime: Madžarević", "JMBG: 12345678", "Broj indeksa: 12345", "Godina studija: 3");

    foreach($student as $val){
        echo $val . " ";
    }
    echo "</br></br>";


    $auto = array(
        "Citroen" => array(
            "tip_automobila" => "C5",
            "kubikaža" => "2200",
            "boja" => "Candy red",
            "godina_proizvodnje" => "2016",
            "motor" => "Dizel"

        ),
        "Mercedes" => array(
            "tip_automobila" => "E klasa",
            "kubikaža" => "3000",
            "boja" => "Crna",
            "godina_proizvodnje" => "2019",
            "motor" => "Benzin"

        )
    );
        foreach($auto["Citroen"] as $prop => $val){
            echo "$prop : $val</br>";
        }
        echo "</br>";
        echo "Tip Automobila: ". $auto["Mercedes"]["tip_automobila"] . "</br>";
        echo "Kubikaža: ". $auto["Mercedes"]["kubikaža"] . "</br>";
        echo "Boja: ". $auto["Mercedes"]["boja"] . "</br>";
        echo "Godina proizvodnje: ". $auto["Mercedes"]["godina_proizvodnje"] . "</br>";
        echo "Motor: ". $auto["Mercedes"]["motor"] . "</br>";

    function kvadrat($broj){
        return $broj * $broj;
    }
    echo "</br>";
    echo "Kvadrat od broja 55 je: " . kvadrat(55);

    echo "</br>";

    class Kupac{
        public $_ime;
        public $_prezime;

        function __construct($ime, $prezime){
            $this->_ime = $ime;
            $this->_prezime = $prezime;
        }

        function setIme($ime){
            $this->_ime = $ime;
        }

        function setPrezime($prezime){
            $this->_prezime = $prezime;
        }

        function ispisiKupca(){
            echo "Kupac je " . $this->_ime . " " . $this->_prezime . ".";
        }

    }

    $kupac1 = new Kupac("Karlo", "Madzarevic");
    $kupac1->ispisiKupca();

















?>
